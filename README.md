## CGI Web App

*A CGI based web application framework, for those who are unable to use WSGI.*

A rudimentary example of usage can be found in the `ExampleSite` folder.