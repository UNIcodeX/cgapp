#!/usr/bin/env python
# -*- coding: UTF-8 -*-
from .__init__ import *

import os
from base64 import b64decode
'''
|   <DEPRECATED>
|   Python 3 handles strings explicitly and more information is below
'''
# import msvcrt
# msvcrt.setmode (0, os.O_BINARY) # stdin  = 0
# msvcrt.setmode (1, os.O_BINARY) # stdout = 1
'''
|   </DEPRECATED>
|   For this to work, the python cgi handler mapping must be set to:
|       executable: c:\Python34\python.exe -u %s %s
|
|   The "-u" switch tells the interpreter to use unbuffered binary standard output
|
|   From the python help:
|       unbuffered binary stdout and stderr, stdin always buffered;
|       also pythonunbuffered=x
|
|   Thanks to Knio (http://stackoverflow.com/a/4517294)
|   and user1543257 (http://stackoverflow.com/a/31496429) for this information!
'''

# handle URL string data
form = cgi.FieldStorage()

file_to_serve = ''
if form.getvalue('F'):
    file_to_serve = form.getvalue("F")

if not file_to_serve:
    sys.exit(1)
else:
    strFullPath = os.path.join(os.getcwd(), 'FWSApplicationSystem', 'uploads', file_to_serve)
    if strFullPath:
        import io
        print("content-type: application/octet-stream")
        print(('Content-Disposition: attachment; filename="{file_to_serve}"'.format(**locals())))
        print('')
        with io.open(sys.stdout.fileno(), 'wb') as outf:
            with open(strFullPath, 'rb') as inf:
                outf.write(inf.read())
