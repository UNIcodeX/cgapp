# STDLIB IMPORTS
import os
import cgi
import sys
import bcrypt
import redis
import datetime

rdb = redis.StrictRedis(host='localhost', port=6379, db=0)


# APP IMPORTS
from .config import Config
config = Config()

config.update({'LDAP_DOMAIN': 'LADELTA',
               'DEBUG': True})

if config['DEBUG']:
    import cgitb
    cgitb.enable(display=1, logdir='./')


def set_cookies(cookie_jar):
    """
    Parameter cookie_jar takes a dict() of cookies to be set, then sets them. This should be called as close to the top of the script as possible.
    :param cookie_jar: dict()
    :return:
    """
    if not cookie_jar:
        cookie_jar = dict()

    for cookie in cookie_jar:
        value = cookie_jar[cookie]
        print("Set-Cookie:{cookie} = {value};".format(**locals()))


def get_cookies():
    cookie_jar = dict()
    if 'HTTP_COOKIE' in os.environ:
        for cookie in map(str.strip, str.split(os.environ['HTTP_COOKIE'], ';')):
            (key, value) = str.split(cookie, '=')
            cookie_jar[key] = value

    return cookie_jar


def test_cookies():
    cookies = get_cookies()
    strHTML = ""
    if cookies:
        for cookie in cookies:
            value = cookies[cookie]
            strHTML += 'Cookie {cookie} set to "{value}"\n'.format(**locals())
    else:

        set_cookies({'username': 'sometwo',
                     'token': 'sometoken'})

        strHTML = "Cookies set. Refresh page to test retrieval."

    print()
    print(strHTML)


from jinja2 import Environment, PackageLoader, select_autoescape

env = Environment(
    loader=PackageLoader('dummy_module', 'templates'),
    autoescape=select_autoescape(['html', 'xml'])
)


def render_template(name, *args, **kwargs):
    template = env.get_template(name)
    print()
    return print(template.render(*args, **kwargs))


form = cgi.FieldStorage()

cookies = None
try:
    cookies = get_cookies()
except:
    pass

strUsername    = ''
strPassword    = ''
strHashedToken = ''

if form.getvalue('U'):
    strUsername = form.getvalue('U')

if form.getvalue('P'):
    strPassword = form.getvalue('P')

if form.getvalue('T'):
    strHashedToken = form.getvalue('T')

def render_HTML(strTitle='', strBody='', strJS=''):
    strHTML = """
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>{strTitle}</title>
    
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
    </head>
    <body>
    <div class="container col-12">
    """.format(**locals())

    strHTML += strBody

    strHTML += """
    </div>
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    """

    if strJS:
        strHTML += """
        <script type="text/javascript">
        """
        strHTML += strJS
        strHTML += """
        </script>"""

    strHTML += """
    </body>
    </html>
    """

    print('')
    print(strHTML)


def debug(msg):
    print()
    print(msg)


def login_required(next=""):
    strNext      = ""
    strNextInput = ""
    strBody      = ""
    strJS        = ""
    login_page   = config['LOGIN_PAGE']

    if next:
        strNext = '?next=' + next
    else:
        if form.getvalue('next'):
            strNext = '?next=' + form.getvalue('next')

    if cookies:
        if 'token' in cookies:
            token = cookies['token']
            if token:
                user = Auth()
                user.check_token(cookies['token'])
                if user.is_authorized:
                    return True
                else:
                    strJS = """
                    window.location.assign('{login_page}{strNext}');
                    """.format(**locals())
                    return render_HTML(strJS=strJS)
            else:
                strJS = """
                window.location.assign('{login_page}{strNext}');
                """.format(**locals())
                return render_HTML(strJS=strJS)
        else:
            strJS = """
            window.location.assign('{login_page}{strNext}');
            """.format(**locals())
            return render_HTML(strJS=strJS)
    else:
        strJS = """
        window.location.assign('{login_page}');
        """.format(**locals())
        return render_HTML(strJS=strJS)

    return render_HTML(strBody="Something went wrong in the authentication process.")


class Auth():
    def __init__(self):
        self.username        = ''
        self.password        = ''
        self.is_authorized   = bool()
        self.hashed_token    = ''
        self.redis_token     = rdb.get('token_raw')

    def check_token(self, hashed_token):
        if not rdb.get('token_raw'):
            self.is_authorized = False
            return False

        try:
            bcrypt.checkpw(rdb.get('token_raw'), hashed_token.encode('utf-8'))
        except:
            self.is_authorized = False
        else:
            self.is_authorized = True
            self.hashed_token = hashed_token

    def authenticate(self, username='', password='', domain=''):
        if not domain:
            try:
                domain = config['LDAP_DOMAIN']
            except:
                raise Exception("LDAP domain must be set for domain authentication.")
        authenticated = bool()
        if username and password:
            try:
                if 'win' in str(sys.platform).lower():
                    import win32security
                    token = win32security.LogonUser(
                        username,
                        domain,
                        password,
                        win32security.LOGON32_LOGON_INTERACTIVE,
                        win32security.LOGON32_PROVIDER_DEFAULT)
                    authenticated = bool(token)
                elif 'linux' in str(sys.platform).lower():
                    import ldap
                    conLDAP = ldap.initialize('ldap://' + domain)
                    conLDAP.protocol_version = 3
                    conLDAP.set_option(ldap.OPT_REFERRALS, 0)
                    token = conLDAP.simple_bind_s(username + '@' + domain, password)
                    authenticated = bool(token)
            except Exception:
                self.is_authorized = False
                self.hashed_token = ''
                return False
        if authenticated:
            self.username = username
            self.create_token()
            return True
        return False

    def create_token(self):
        self.redis_token = os.urandom(10)
        rdb.set('token_raw', self.redis_token)
        rdb.expireat('token_raw', (datetime.datetime.now() + datetime.timedelta(days=1)))
        self.hashed_token = bcrypt.hashpw(self.redis_token.replace(b'\x00', b''), bcrypt.gensalt()).decode('utf-8')
        set_cookies(cookie_jar={'username': self.username,
                                'token': self.hashed_token})
        self.is_authorized = True

    def __repr__(self):
        return "is_authenticated: {self.is_authorized}\ntoken: {self.hashed_token}".format(**locals())


def redirect(s):
    strJS = 'window.location.assign("{s}");'.format(**locals())
    render_HTML(strJS=strJS)