from sqlalchemy import create_engine, Column, Integer, String, ForeignKey
from sqlalchemy.ext.declaration import declarative_base

engine = create_engine('sqlite://database/FWSApps.db', echo=True)

Base = declarative_base()

class applicants(Base):
    __tablename__ = 'applicants'

    id          = Column('id', Integer, primary_key=True)
    first_name  = Column('first_name', String)
    last_name   = Column('last_name', String)
    middle_name = Column('middle_name', String)
    email       = Column('email', String)


Base.metadata.create_all(bind=engine)
