# !/usr/bin/env python
# -*- coding: UTF-8 -*-

from CGApp import *

__author__ = 'Jared Blake Fields <jaredfields@ladelta.edu>'

strTitle = "Admin Page"
strBody  = ""
strJS    = ""

login_required(next=os.path.basename(__file__))

if cookies:
    if 'username' in cookies:
        strUsername = cookies['username']
        strToken    = cookies['token']
        strBody = """
        <center>
            <div class="row col-4 justify-content-center">
                <p>Greetings, {strUsername}.</p>
                
                <p>Your login token:<br/>
                {strToken}
                </p>
            </div>
        </center>""".format(**locals())
else:
    strBody = 'Not authorized.'


render_template('admin.html', **locals())
