from CGApp import *

strTitle    = "Logout"
strBody     = "Logged out. Redirecting to Login page."
strJS       = "window.location.assign('login.py');"

set_cookies({'username': '',
             'token': ''
             })

render_HTML(strTitle=strTitle, strBody=strBody, strJS=strJS)
