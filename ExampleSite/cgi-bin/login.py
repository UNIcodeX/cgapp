#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from CGApp import *

__author__ = 'Jared Blake Fields <jaredfields@ladelta.edu>'

strTitle = "Login"
strBody = ""
strJS = ""

strNext = ""

if form.getvalue('next'):
    strNext = form.getvalue('next')

if not strUsername and not strPassword:
    strBody += """
    <style>
    .input {
        border: solid black 1px;
        border-bottom-right-radius: 2px;
        border-radius: 10px;
        padding-left: 10px;
        padding-right: 30px;
    }
    </style>
    <center>

    <div id="outer_login" style="text-align:center;height:220px;position:absolute;width:99%;display:table;">
    <div style="display:table-cell;vertical-align:middle;text-align:center;width:50%;margin:0 auto;">
            <form action="" method="POST" style="width:97.5%;border-radius:10px;">
                <div class="header"><font style="font-size: 30px;">Application Sign In</font><hr style="width:330px;border:solid 1px;"></div>
                <div class="form-group" >
                Username: <input type="text" name="U" class="input" required/> <br/>
                </div>
                <div class="form-group" >
                Password: <input type="password" name="P" class="input" required /><br/>
                </div>
                <input type="submit" name="submit" value="LOGIN" class="btn btn-info"/>
            </form>
        </div>
    </div>
    </div>
    </div>
    </center>
    """
    strJS = """
    document.getElementById('outer_login').setAttribute('style', 'height:'+window.innerHeight+'px;position:absolute;width:99%;display:table;text-align:center;')"""
else:
    user = Auth()
    user.authenticate(username=strUsername, password=strPassword)
    if user.is_authorized:
        # extended_info = get_user_info(strUsername)
        # Since get_user_info is supposed to return information from a DB query, based on the username,
        # this is a hack to simulate that action for the ExampleSite app
        extended_info = {'Name': 'someone'}
        if extended_info:
            cookies = {'username': user.username,
                       'token': user.hashed_token}

            if type(extended_info) is dict:
                cookies.update(extended_info)

            set_cookies(cookies)

            if strNext:
                strJS += """
                window.location.assign('{strNext}');
                """.format(**locals())
            else:
                strJS += """
                window.location.assign('admin.py');
                """.format(**locals())
    else:
        strJS += """
        window.location.assign('login.py');
        """.format(**locals())

render_HTML(strTitle=strTitle, strBody=strBody, strJS=strJS)
